#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "colors.h"

char *userName;
char *city;
char *state;
/*frb stands for:
File
Read
Buffer
it's used to store the file text
Yhea... I know, I could use the pointer variable
But I don't to cause a memory leak*/
char *frb;
char *country;
char *address;
char *zipCode;
/*Declares the name that is going to be used to read or wire the file.
Yes yes... it could be a 2d array but for now it will be a 1d array.
it will work like this:
fprt(fileName,"w");
or:
fprt(fileName,"r");*/
char *fileName;
/*When saving the information to a file we are gonna use the ponier variable.
Because then we don't have to make a write file function for.
evry char variable.
it will work like this:
pointer = zipCode;(or any other variable)*/
char *pointer;
char *age;
/*gunc stands for:
getUserChoice
Number
it's for the user selection promption in the getUserChoice funnction*/
char *gucn;
const float ver_float = 1.4;

void getuserinfo(void);
/*Records the text from a file to frb*/
void fileRead(void);
/*Displays the content in the frb to the user*/
void fileReadShow(void);
void fileCreate(void);
void clear(void);/*Cleans the text on the terminal emulator*/
void showInfo(void);
/*It's part of the beta 1.4 update it uses a more friendily UI
Somthing like this:
1 For writing info

2 For reding info*/
void  getUserChoice(void);
void initFuncs(void);

int main(int argc, char *argv[])
{
    userName = (char *) malloc(101);
    pointer = (char *) malloc(152);
    state = (char *) malloc(51);
    age = (char *) malloc(3);
    frb = (char *) malloc(152);
    gucn = (char *) malloc(2);
    country = (char *) malloc(51);
    zipCode = (char *) malloc(12);
    address = (char *) malloc(151);
    city = (char *) malloc(51);
    userName[100] = '\0';
    city[50] = '\0';
    pointer[151]= '\0';
    zipCode[11] = '\0';
    address[150] = '\0';
    age[2] = '\0';
    state[50] = '\0';
    gucn[1] = '\0';
    country[50] = '\0';
    if(argc == 1)
    {
        initFuncs();
    }
    else if (argc == 2)
    {
		if(strcmp(argv[1], "-h") == 0)
		{
			printf(BBLU"BSUIC"RES" is a data collector.\n");
            printf("To show the version use the \"-v\" flag\n");
            printf("To read the recorded information use the \"-r\" flag\n");
            printf(BYEL"Beta %1.1f update notes:\n"RES"*Improved CLI interface\n",ver_float);
			exit(0);
		}
        if(strcmp(argv[1], "-r") == 0)
		{
			fileReadShow();
			exit(0);
		}
		if(strcmp(argv[1], "-v") == 0)
		{
			/*printf(BGRN"Better\nSimple\nUser\nInput\nColector\nbeta 0.2\n"RES);*/
			printf(BBLU"BSUIC\n"RES"Interface:"BYEL"CLI\n"RES"Beta %1.1f\n",ver_float);
            exit(0);
        }
    }
    else
    {
        printf(BRED"Argument error!\nPlease insert the correct argument\n"RES);
    }
    free(userName);
    free(zipCode);
    free(country);
    free(city);
    free(address);
    free(state);
    free(age);
    return 0;
}

void initFuncs(void)
{
    clear();
    getUserChoice();
}

void showInfo(void)
{
    //clear();
    printf("Your name:%s\n",userName);
    fileName = "name.txt";
    pointer = userName;
    fileCreate();
    printf("Your age is:%s\n",age);
    fileName = "age.txt";
    pointer = age;
    fileCreate();
    printf("your cn is:%s\n",country);
/*
NOTE: the code bellow is old code but now I fixed with the new CLI ui.
*/
/*    printf("Your country is:");
    for(int i = 2;i < sizeof(country);i++)
    {
        printf("%c",country[i]);
    }
    printf("\n");
*/
    fileName = "cn.txt";
    pointer = country;
    fileCreate();
/*  printf(BRED"DEBUG_size_of_coutry:%lu\n"RES,sizeof(country));*/
    printf("Your state is:%s\n",state);
    fileName = "state.txt";
    pointer = state;
    fileCreate();
    printf("Your city is:%s\n",city);
    fileName = "city.txt";
    pointer = city;
    fileCreate();
    printf("Your zip code is:%s\n",zipCode);
    fileName = "zc.txt";
    pointer = zipCode;
    fileCreate();
    printf("Your address is:%s\n",address);
    fileName = "add.txt";
    pointer = address;
    fileCreate();
}

void getUserChoice(void)
{
    printf("Type "BBLU"1 "RES"for writing your information.\n");
    printf("Type "BYEL"2 "RES"for reading your information.\n");
    fgets(gucn,sizeof(gucn),stdin);
//    printf(BGRN"the content of gucn is: %c\n"RES,gucn[0]);
    if(gucn[0] == '1')
    {
        getuserinfo();
        showInfo();
    }
    else if(gucn[0] == '2')
    {
        fileReadShow();
    }
    else
    {
        printf(BRED"You typed the the wrong number!\n"RES);
    }
}

void getuserinfo(void)
{
/*  printf("DEBUG:%lu\n",sizeof(country));*/
/*
NOTE: The beta 1.2 UI is not user frendily so I gonna change it.
*/
    printf("The user's name,age,country,state,city,zip code,address\nLike this:\n");
    printf(BGRN"Kevin\n28\nUSA\nMato Grosso\nNew Yourk City\n12345\nJardimm Belo");
    printf(" Elmer Street 190.\n"RES);
    printf(BRED"Remenber to add a . at the end of the address\n"RES);
    scanf("%[^\n]\n%[^\n]\n%[^\n]\n%[^\n]\n%[^\n]\n%[^\n]\n%[^.]\n",userName,age,country,state,city,zipCode,address);
}

void fileCreate(void)
{
    FILE * fprt;
    fprt = fopen(fileName,"w");
    fputs(pointer,fprt);
    fclose(fprt);
}

void fileReadShow(void)
{
    printf("Your name is:");
    fileName = "name.txt";
    fileRead();
    printf("%s\n",frb);
    printf("Your age is:");
    fileName = "age.txt";
    fileRead();
    printf("%s\n",frb);
    printf("Your country is:");
    fileName = "cn.txt";
    fileRead();
    printf("%s\n",frb);
    printf("Your state is:");
    fileName = "state.txt";
    fileRead();
    printf("%s\n",frb);
    printf("Your city is:");
    fileName = "city.txt";
    fileRead();
    printf("%s\n",frb);
    printf("Your zip code is:");
    fileName = "zc.txt";
    fileRead();
    printf("%s\n",frb);
    printf("Your address is:");
    fileName = "add.txt";
    fileRead();
    printf("%s\n",frb);
}

void fileRead(void)
{
    FILE * rft;
    rft = fopen(fileName,"r");
    fgets(frb,151,(FILE *)rft);
    fclose(rft);
}

void clear(void)
{
    system("clear");
}